FROM dailambda/opam:1.1.0
MAINTAINER Jun FURUSE <jun.furuse@dailambda.jp>
RUN apt install -y gcc
RUN opam update
RUN opam switch create 4.09.1
ENV OPAM_SWITCH_PREFIX /root/.opam/4.09.1
ENV CAML_LD_LIBRARY_PATH "/root/.opam/4.09.1/lib/stublibs:Updated by package ocaml"
ENV OCAML_TOPLEVEL_PATH /root/.opam/4.09.1/lib/toplevel
ENV MANPATH :/root/.opam/4.09.1/man
ENV PATH /root/.opam/4.09.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
